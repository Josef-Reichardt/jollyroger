# Jollyroger

The new development of this project is now hosted at https://gitlab.com/CptS/jollyroger

The latest stage of development here can be found in the 
[`legacy`](https://bitbucket.org/Josef-Reichardt/jollyroger/src/legacy/) branch.
